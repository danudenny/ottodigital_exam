package main

import (
	"ottodigital/domain/v1/ottodigital/transaction"
	"ottodigital/domain/v1/ottodigital/user"
	"ottodigital/middlewares"
	"ottodigital/utils"

	"ottodigital/utils/config"

	"github.com/labstack/echo/v4"
)

func main() {
	e := echo.New()
	logger := utils.Logger()
	conf, _ := config.LoadConfig(".")
	db, err := config.DBConn()
	if err != nil {
		e.Logger.Error(err)
	}

	if err = db.Ping(); err != nil {
		logger.Fatal("unable to reach database: %v", err)
	}
	logger.Info("database connected")

	e.POST("/user/create", user.CreateUser)
	e.POST("/user/login", user.Login)
	e.GET("/user/profile", user.Profile, middlewares.IsAuthenticated)

	e.POST("/transaction/create", transaction.CreateTransaction, middlewares.IsAuthenticated)

	logger.Info(e.Start(":" + conf.ServerPort))
}
