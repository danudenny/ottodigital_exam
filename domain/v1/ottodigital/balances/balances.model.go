package balances

type Balance struct {
	ID      uint    `json:"id,omitempty"`
	Balance float32 `json:"balance,omitempty"`
	UserID  uint    `json:"user_id,omitempty"`
}
