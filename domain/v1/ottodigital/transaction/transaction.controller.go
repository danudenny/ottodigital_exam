package transaction

import (
	"net/http"
	"ottodigital/middlewares"
	"ottodigital/utils"

	"github.com/labstack/echo/v4"
)

func CreateTransaction(c echo.Context) error {
	logger := utils.Logger()
	trh := TransactionHistory{}
	c.Bind(&trh)
	userId := middlewares.ExtractTokenID(c)
	save, err := SaveTransaction(userId, trh)
	if err != nil {
		logger.Error(err)
	}

	return c.JSON(http.StatusOK, save)
}
