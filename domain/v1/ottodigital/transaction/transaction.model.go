package transaction

import (
	"fmt"
	"net/http"
	"ottodigital/utils"
	"ottodigital/utils/config"
	"time"
)

type Transaction struct {
	ID      uint               `json:"id,omitempty"`
	UserID  uint               `json:"user_id,omitempty"`
	Number  string             `json:"number,omitempty"`
	History TransactionHistory `json:"history,omitempty"`
}

type TransactionHistory struct {
	ID              uint      `json:"id,omitempty"`
	TransactionID   uint      `json:"transaction_id,omitempty"`
	TransactionDate time.Time `json:"transaction_date,omitempty"`
	Amount          float32   `json:"amount,omitempty"`
	State           State     `json:"state,omitempty"`
	DeclinedReason  string    `json:"declined_reason,omitempty"`
}

type State string

const (
	StatePending  State = "pending"
	StateApproved State = "approved"
	StateDeclined State = "declined"
)

func SaveTransaction(id uint, t TransactionHistory) (utils.Response, error) {
	var resp utils.Response
	logger := utils.Logger()
	conn, _ := config.DBConn()
	trx := Transaction{}

	defer conn.Close()

	prepareStmt := `INSERT INTO transactions (user_id, number) VALUES ($1, $2) RETURNING id`

	err := conn.QueryRow(prepareStmt,
		&id,
		fmt.Sprintf("TRX-%d", time.Now().Unix()),
	).Scan(&trx.ID)
	if err != nil {
		logger.Error(err)
	}

	history := SaveTrxHistory(
		trx.ID,
		t.Amount,
	)

	resp.Code = http.StatusOK
	resp.Message = "Success Create Transaction"
	resp.Data = map[string]interface{}{
		"trx_id":  &trx.ID,
		"history": history,
	}

	return resp, nil
}

func SaveTrxHistory(trxId uint, amount float32) error {
	logger := utils.Logger()
	conn, _ := config.DBConn()
	trx := TransactionHistory{}

	defer conn.Close()

	prepareStmt := `INSERT INTO transaction_histories (transaction_id, transaction_date, amount, state) VALUES ($1, $2, $3, $4) RETURNING id`

	err := conn.QueryRow(prepareStmt,
		&trxId,
		time.Unix(time.Now().Unix(), 0),
		&amount,
		StatePending,
	).Scan(&trx.ID)
	if err != nil {
		logger.Error(err)
	}

	return nil
}
