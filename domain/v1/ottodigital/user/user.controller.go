package user

import (
	"net/http"
	"ottodigital/middlewares"
	"ottodigital/utils"

	"github.com/labstack/echo/v4"
	_ "github.com/lib/pq"
)

func CreateUser(c echo.Context) error {
	logger := utils.Logger()

	user := Users{}
	c.Bind(&user)

	result, err := SaveUser(&user)
	if err != nil {
		logger.Error(err)
	}

	return c.JSON(http.StatusOK, result)
}

func Login(c echo.Context) error {
	logger := utils.Logger()
	userLogin := Users{}
	c.Bind(&userLogin)

	res, err := CheckLogin(userLogin.Username, userLogin.Password)
	if err != nil {
		logger.Error("login failed")
		return c.JSON(http.StatusInternalServerError, map[string]string{
			"messages": err.Error(),
		})
	}

	return c.JSON(http.StatusOK, res)
}

func Profile(c echo.Context) error {
	logger := utils.Logger()
	userId := middlewares.ExtractTokenID(c)
	userData, err := CheckProfile(uint(userId))

	if err != nil {
		logger.Error(err.Error())
	}
	return c.JSON(http.StatusOK, userData)

}
