package user

import (
	"database/sql"
	"net/http"
	"ottodigital/domain/v1/ottodigital/balances"
	"ottodigital/domain/v1/ottodigital/transaction"
	"ottodigital/middlewares"
	"ottodigital/utils"
	"ottodigital/utils/config"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Users struct {
	ID          uint                    `json:"id,omitempty"`
	Name        string                  `json:"name,omitempty"`
	Username    string                  `json:"username,omitempty"`
	Email       string                  `json:"email,omitempty"`
	Phone       string                  `json:"phone,omitempty"`
	Password    string                  `json:"password,omitempty"`
	Address     string                  `json:"address,omitempty"`
	Balance     balances.Balance        `json:"balance,omitempty"`
	Transaction transaction.Transaction `json:"transaction,omitempty"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	return string(bytes), err
}

func ComparePassword(hashedPassword, plainPassword string) (bool, error) {
	logger := utils.Logger()
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(plainPassword))
	if err != nil {
		logger.Error(err)
	}

	return true, nil
}

func SaveUser(u *Users) (utils.Response, error) {
	var resp utils.Response
	logger := utils.Logger()
	conn, _ := config.DBConn()
	hashPassword, err := HashPassword(u.Password)
	if err != nil {
		logger.Error(err)
	}

	defer conn.Close()

	prepareStmt := `INSERT INTO users (name, username, email, phone, address, password) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	err = conn.QueryRow(prepareStmt,
		&u.Name,
		&u.Username,
		&u.Email,
		&u.Phone,
		&u.Address,
		hashPassword,
	).Scan(&u.ID)
	if err != nil {
		logger.Error(err)
	}

	resp.Code = http.StatusOK
	resp.Message = "Success Create User"
	resp.Data = &u.ID
	return resp, nil
}

func CheckLogin(username, password string) (*utils.Response, error) {
	logger := utils.Logger()
	var resp utils.Response
	conn, _ := config.DBConn()
	defer conn.Close()

	var user Users
	var plainPass string
	//GetUsername row
	prepareStmt := `SELECT id, username, password FROM users WHERE username = $1`
	err := conn.QueryRow(prepareStmt, username).Scan(
		&user.ID,
		&user.Username,
		&plainPass,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			logger.Error("user not found")
			return nil, err
		}
		logger.Error(err)
		return nil, err
	}

	match, err := ComparePassword(plainPass, password)
	if !match {
		logger.Error("password not match")
		return nil, err
	}

	token, err := middlewares.CreateToken(map[string]interface{}{
		"username":   user.Username,
		"id":         user.ID,
		"login_time": time.Now(),
	}, user.ID)
	if err != nil {
		logger.Error(err)
	}

	resp.Code = http.StatusOK
	resp.Message = "Login Success"
	resp.Data = map[string]interface{}{
		"token": token,
	}

	return &resp, err

}

func CheckProfile(id uint) (*utils.Response, error) {
	logger := utils.Logger()
	var resp utils.Response
	conn, _ := config.DBConn()
	defer conn.Close()

	var user Users

	prepareStmt := `
		SELECT us.id, us.name, us.username, us.email, us.phone, us.address, t.number, th.transaction_date, th.amount, th.state FROM users us
		LEFT JOIN transactions t ON t.user_id = us.id
		LEFT JOIN transaction_histories th ON th.transaction_id = t.id
		WHERE us.id = $1
	`
	err := conn.QueryRow(prepareStmt, id).Scan(
		&user.ID,
		&user.Name,
		&user.Username,
		&user.Email,
		&user.Phone,
		&user.Address,
		&user.Transaction.Number,
		&user.Transaction.History.TransactionDate,
		&user.Transaction.History.Amount,
		&user.Transaction.History.State,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			logger.Error("user not found")
			return nil, err
		}
		logger.Error(err)
		return nil, err
	}

	resp.Code = http.StatusOK
	resp.Message = "Success Fetch Data"
	resp.Data = user

	return &resp, err
}
