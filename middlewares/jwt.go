package middlewares

import (
	"encoding/json"
	"fmt"
	"ottodigital/utils"
	"ottodigital/utils/config"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func CreateToken(payload interface{}, id uint) (string, error) {
	now := time.Now().UTC()

	conf, _ := config.LoadConfig(".")

	claims := make(jwt.MapClaims)
	claims["sub"] = payload
	claims["user_id"] = id
	claims["exp"] = time.Now().Add(time.Hour * 2).Unix()
	claims["iat"] = now.Unix()
	claims["nbf"] = now.Unix()

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(conf.JWTSecret))

	if err != nil {
		return "", fmt.Errorf("create: sign token: %w", err)
	}

	return token, nil
}

func TokenValid(c echo.Context) error {
	conf, _ := config.LoadConfig(".")
	tokenString := ExtractToken(c)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(conf.JWTSecret), nil
	})
	if err != nil {
		return err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		Pretty(claims)
	}
	return nil
}

func ExtractToken(c echo.Context) string {
	keys := c.Request().URL.Query()
	token := keys.Get("token")
	if token != "" {
		return token
	}
	bearerToken := c.Request().Header.Get("Authorization")
	if len(strings.Split(bearerToken, " ")) == 2 {
		return strings.Split(bearerToken, " ")[1]
	}
	return ""
}

func ExtractTokenID(c echo.Context) uint {
	conf, _ := config.LoadConfig(".")
	tokenString := ExtractToken(c)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(conf.JWTSecret), nil
	})
	if err != nil {
		return 0
	}
	claims, ok := token.Claims.(jwt.MapClaims)

	if ok && token.Valid {
		uid, err := strconv.ParseUint(fmt.Sprintf("%.0f", claims["user_id"]), 10, 32)
		if err != nil {
			return 0
		}
		return uint(uid)
	}
	return 0
}

func Pretty(data interface{}) {
	logger := utils.Logger()
	b, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		logger.Error(err)
		return
	}

	fmt.Println(string(b))
}

var IsAuthenticated = middleware.JWTWithConfig(
	middleware.JWTConfig{
		SigningKey: []byte("thisissecret"),
	},
)
