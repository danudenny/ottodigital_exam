/*
 Navicat Premium Data Transfer

 Source Server         : pg_local
 Source Server Type    : PostgreSQL
 Source Server Version : 140004
 Source Host           : localhost:5432
 Source Catalog        : ottodigital
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140004
 File Encoding         : 65001

 Date: 01/09/2022 17:55:43
*/


-- ----------------------------
-- Sequence structure for balances_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "balances_id_seq";
CREATE SEQUENCE "balances_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for transaction_histories_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transaction_histories_id_seq";
CREATE SEQUENCE "transaction_histories_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for transactions_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "transactions_id_seq";
CREATE SEQUENCE "transactions_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "users_id_seq";
CREATE SEQUENCE "users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for balances
-- ----------------------------
DROP TABLE IF EXISTS "balances";
CREATE TABLE "balances" (
  "id" int8 NOT NULL DEFAULT nextval('balances_id_seq'::regclass),
  "balance" numeric,
  "user_id" int8,
  "created_at" timestamptz(6),
  "updated_at" timestamptz(6)
)
;

-- ----------------------------
-- Records of balances
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for transaction_histories
-- ----------------------------
DROP TABLE IF EXISTS "transaction_histories";
CREATE TABLE "transaction_histories" (
  "id" int8 NOT NULL DEFAULT nextval('transaction_histories_id_seq'::regclass),
  "transaction_id" int8,
  "transaction_date" timestamptz(6),
  "amount" numeric,
  "state" text COLLATE "pg_catalog"."default",
  "declined_reason" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of transaction_histories
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for transactions
-- ----------------------------
DROP TABLE IF EXISTS "transactions";
CREATE TABLE "transactions" (
  "id" int8 NOT NULL DEFAULT nextval('transactions_id_seq'::regclass),
  "user_id" int8,
  "number" text COLLATE "pg_catalog"."default",
  "created_at" date DEFAULT now(),
  "updated_at" date DEFAULT now()
)
;

-- ----------------------------
-- Records of transactions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "users";
CREATE TABLE "users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(50) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(20) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6) NOT NULL DEFAULT now(),
  "updated_at" timestamp(6) NOT NULL DEFAULT now()
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "balances_id_seq"
OWNED BY "balances"."id";
SELECT setval('"balances_id_seq"', 1, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transaction_histories_id_seq"
OWNED BY "transaction_histories"."id";
SELECT setval('"transaction_histories_id_seq"', 1, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "transactions_id_seq"
OWNED BY "transactions"."id";
SELECT setval('"transactions_id_seq"', 6, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"users_id_seq"', 3, true);

-- ----------------------------
-- Primary Key structure for table balances
-- ----------------------------
ALTER TABLE "balances" ADD CONSTRAINT "balances_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table transaction_histories
-- ----------------------------
ALTER TABLE "transaction_histories" ADD CONSTRAINT "transaction_histories_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table transactions
-- ----------------------------
ALTER TABLE "transactions" ADD CONSTRAINT "transactions_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table transaction_histories
-- ----------------------------
ALTER TABLE "transaction_histories" ADD CONSTRAINT "fk_transactions_history" FOREIGN KEY ("transaction_id") REFERENCES "transactions" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
