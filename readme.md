# Ottodigital Backend Test
## Stack :
- Golang - Gin Framework
- Gorm as ORM
- Mysql Databases

## Todo :
- [x] Authentication
   - [x] Register
   - [x] Login
   - [x] Profile
- [x] Transaction
   - [x] Transaction Create
   - [ ] Transaction COnfirmation
   - [x] Transaction History
- [ ] Topup

