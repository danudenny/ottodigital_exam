package config

import (
	"database/sql"
	"fmt"
	"ottodigital/utils"
)

func DBConn() (*sql.DB, error) {
	logger := utils.Logger()
	conf, _ := LoadConfig(".")
	conn := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
		conf.DatabaseHost,
		conf.DatabasePort,
		conf.DatabaseUsername,
		conf.DatabasePassword,
		conf.DatabaseName,
	)
	db, err := sql.Open("postgres", conn)
	if err != nil {
		logger.Error(err)
	}
	return db, err
}
