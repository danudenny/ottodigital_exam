package utils

type Response struct {
	Code    int         `json:"code" form:"error_code"`
	Message string      `json:"message" form:"message"`
	Data    interface{} `json:"data"`
}
